<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TC003_BlogAndWordle</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>01f16591-538d-4718-8f6a-f34381d238b5</testSuiteGuid>
   <testCaseLink>
      <guid>63135fe0-0c8c-4557-b5e5-d1321f56b886</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Blog</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1aa0c415-6538-4d7e-83fe-2daaef3add47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Wordle</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
